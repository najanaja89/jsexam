var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var moment = require('moment');
var todoSchema = new Schema({
  title: String,
  status: Boolean,
  body: String,
  date: {
    type: Date,
    default: Date.now
  },
})

todoSchema
  .virtual('date_string')
  .get(function() {
    return moment(this.date).format('DD.MM.YYYY HH:mm')
  });
var Todo = mongoose.model('Todo', todoSchema);

module.exports = Todo;
