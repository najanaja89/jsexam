var express = require('express');
var cors = require('cors');
var app = express();
var mongoose = require('mongoose');
var bodyParser = require('body-parser')
mongoose.connect('mongodb://localhost/todo');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'))


app.set('views', './views');
app.set('view engine', 'jade');
app.use(cors());
app.use(express.static('public'));
app.use(bodyParser.json({
  limit: '50mb'
}));
app.use(bodyParser.urlencoded({
  extended: false,
  limit: '50mb',
  parameterLimit: 50000
}));

var router = require('./routes/todo');
app.use('/', router);

app.listen(3001, function() {
  console.log('Example app listening on port 3001!');
});
