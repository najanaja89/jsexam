var express = require('express');
var router = express.Router();
var Todo = require('../models/todo')



router.get('/', function(req, res) {
  Todo.find({}, function(err, collection) {
    if (err)
      console.log(err)
    else {
      res.json(collection);
    };
  });
});


router.post('/add', function(req, res) {
  console.log(req.body);
  Todo.create({
    title: req.body.title,
    status: req.body.status,
    body: req.body.body
  }, function(error, created) {
    if (error) console.log(error);
    else res.json(created);
  })
});

router.post('/edit/:id', function(req, res) { //edit экземпляр объекта
  Todo.findOne({
    _id: req.params.id
  }, function(err, edit) {
    if (err) {
      console.log(err);
    } else {
      for (key in req.body) {
        edit[key] = req.body[key];
      }
      edit.save(function(error, saved) {
        if (error) {
          console.log(error);
        } else res.json(saved);
      })
    };
  });
});

module.exports = router;
